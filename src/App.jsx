import { Flex } from "@chakra-ui/react";
import { RiMenuUnfoldLine } from "react-icons/ri";
import { FaUserCircle } from "react-icons/fa";
import { IoChatbubbles, IoChatbubble, IoOptions } from "react-icons/io5";

function App() {
  return (
    <Flex bg="white" h="100vh">
      <Flex w="50px" direction="column" align="center">
        <RiMenuUnfoldLine size={32} color="#959595" />
        <IoChatbubbles size={32} color="#959595" />
        <IoChatbubble size={32} color="#959595" />
        <FaUserCircle size={32} color="#959595" />
        <IoOptions size={32} color="#959595" />
      </Flex>
      <Flex flex="1" direction="column"></Flex>
      <Flex flex="3" bg="gray.300"></Flex>
    </Flex>
  );
}

export default App;
